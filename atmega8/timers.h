#define TIFR_T0_OVERFLOW_FLAG 0b00000001
#define TIFR_T1_OVERFLOW_FLAG 0b00000100
#define TIFR_T1_OUTPUT_COMPARE_A_MATCH_FAG 0b00001000
#define TIFR_T1_OUTPUT_COMPARE_B_MATCH_FAG 0b00010000
#define TIFR_T1_INPUT_CAPTURE_FLAG 0b00100000
#define TIFR_T2_OVERFLOW_FLAG 0b01000000
#define TIFR_T2_OUTPUT_COMPARE_MATCH_FAG 0b10000000

#define TCCR1A_NON_PWM_PORTS_DISCONECTED = 0b00000000
#define TCCR1A_NON_PWM_TOGGLE_PORTS_ON_COMPARE_MATCH = 0b01010000
#define TCCR1A_NON_PWM_CLEAR_PORTS_ON_COMPARE_MATCH = 0b10100000
#define TCCR1A_NON_PWM_SET_PORTS_ON_COMPARE_MATCH = 0b11110000
#define TCCR1A_FORCE_OUTPUT_COMPARE_FOR_CHANEL_A 0b00001000
#define TCCR1A_FORCE_OUTPUT_COMPARE_FOR_CHANEL_B 0b00000100

#define TCCR1A_WAVEFORM_GEN_MODE_NORMAL 0b00000000
#define TCCR1B_WAVEFORM_GEN_MODE_NORMAL 0b00000000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_8BIT 0b00000001
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_8BIT 0b00000000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_9BIT 0b00000010
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_9BIT 0b00000000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_10BIT 0b00000011
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_10BIT 0b00000000

#define TCCR1A_WAVEFORM_GEN_MODE_CTC_1 0b00000000
#define TCCR1B_WAVEFORM_GEN_MODE_CTC_1 0b00001000

#define TCCR1A_WAVEFORM_GEN_MODE_FAST_PWM_8BIT 0b00000001
#define TCCR1B_WAVEFORM_GEN_MODE_FAST_PWM_8BIT 0b00001000

#define TCCR1A_WAVEFORM_GEN_MODE_FAST_PWM_9BIT 0b00000010
#define TCCR1B_WAVEFORM_GEN_MODE_FAST_PWM_9BIT 0b00001000

#define TCCR1A_WAVEFORM_GEN_MODE_FAST_PWM_10BIT 0b00000011
#define TCCR1B_WAVEFORM_GEN_MODE_FAST_PWM_10BIT 0b00001000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_AND_FREQUENCY_CORRECT_1 0b00000000
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_AND_FREQUENCY_CORRECT_1 0b00010000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_AND_FREQUENCY_CORRECT_2 0b00000001
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_AND_FREQUENCY_CORRECT_2 0b00010000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_1 0b00000010
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_1 0b00010000

#define TCCR1A_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_2 0b00000011
#define TCCR1B_WAVEFORM_GEN_MODE_PWM_PHASE_CORRECT_2 0b00010000

#define TCCR1A_WAVEFORM_GEN_MODE_CTC_2 0b00000000
#define TCCR1B_WAVEFORM_GEN_MODE_CTC_2 0b00011000

#define TCCR1A_WAVEFORM_GEN_MODE_FAST_PWM_1 0b00000010
#define TCCR1B_WAVEFORM_GEN_MODE_FAST_PWM_1 0b00011000

#define TCCR1A_WAVEFORM_GEN_MODE_FAST_PWM_2 0b00000011
#define TCCR1B_WAVEFORM_GEN_MODE_FAST_PWM_2 0b00011000

#define TCCR1B_ENABLE_INPUT_CAPTURE_NOISE_CANCELER 0b10000000

#define TCCR1B_INPUT_CAPTURE_EDGE_SELECT_FALLING_EDGE 0b00000000
#define TCCR1B_INPUT_CAPTURE_EDGE_SELECT_RISING_EDGE 0b01000000

#define TCCR1B_T1_STOP 0b00000000
#define TCCR1B_NO_PRESCALING 0b00000001
#define TCCR1B_PRESCALING_CLK_8 0b00000010
#define TCCR1B_PRESCALING_CLK_64 0b00000011
#define TCCR1B_PRESCALING_CLK_256 0b00000100
#define TCCR1B_PRESCALING_CLK_1024 0b00000101
#define TCCR1B_CLK_ON_T1_FALLING_EDGE 0b00000110
#define TCCR1B_CLK_ON_T1_RISING_EDGE 0b00000111

#define TIMSK_T0_ENABLE_OVF_INTERRUPT 0b00000001
#define TIMSK_T1_ENABLE_INPUT_CAPTURE_INTERRUPT 0b00100000
#define TIMSK_T1_ENABLE_O_COMPARE_A_MATCH_INTERRUPT 0b00010000
#define TIMSK_T1_ENABLE_O_COMPARE_B_MATCH_INTERRUPT 0b00001000
#define TIMSK_T1_ENABLE_OVF_INTERRUPT 0b00000100

#define TCCR0_T0_STOP 0b00000000
#define TCCR0_NO_PRESCALING 0b001
#define TCCR0_CLK_8 0b010
#define TCCR0_CLK_64 0b011
#define TCCR0_CLK_256 0b100
#define TCCR0_CLK_1024 0b101
#define TCCR0_CLK_ON_T0_FALLING_EDGE 0b110
#define TCCR0_CLK_ON_T0_RISING_EDGE 0b111


