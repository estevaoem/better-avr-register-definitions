F_CPU = 14745600

timmers = [
    {
        "name": "timmer0",
        "prescalers": [1, 8, 64, 256, 1024],
        "bit_size": 8,
        "specs": [],
    },
    {
        "name": "timmer1",
        "prescalers": [1, 8, 64, 256, 1024],
        "bit_size": 16,
        "specs": [],
    },
]


def timmer_specs():
    for timmer in timmers:
        for prescaler in timmer["prescalers"]:
            time_resolution = prescaler / F_CPU
            max_time_count_in_seconds = time_resolution * (2 ** timmer["bit_size"])
            specs = {
                "prescaler": prescaler,
                "resolution": time_resolution,
                "max_time_count_in_seconds": max_time_count_in_seconds,
            }
            timmer["specs"].append(specs)


timmer_specs()

for timmer in timmers:
    print(timmer["name"])
    for spec in timmer["specs"]:
        print(spec)


def get_maximum_time_count():
    max_time_count = 0
    for timmer in timmers:
        for spec in timmer["specs"]:
            time_count = spec["max_time_count_in_seconds"]
            if time_count > max_time_count:
                max_time_count = time_count
    return max_time_count


def select_best_timmer(time):

    candidate_timmers = []

    for timmer in timmers:
        for spec in timmer["specs"]:
            candidate_timmers.append(
                {
                    "name": timmer["name"],
                    "max_time": spec["max_time_count_in_seconds"],
                    "resolution": spec["resolution"],
                    "specs": spec,
                }
            )

    max_time_s = get_maximum_time_count()

    score = {"score": max_time_s}

    if time <= max_time_s:
        for candidate_timmer in candidate_timmers:
            time_diff = candidate_timmer["max_time"] - time
            ticks_for_desired_T = int(time / candidate_timmer["resolution"])

            score_model = {
                "timmer": candidate_timmer["name"],
                "score": time_diff,
                "ticks_for_desired_T": ticks_for_desired_T,
                "specs": candidate_timmer["specs"],
            }

            if ticks_for_desired_T > 256:
                TCNT = 65535 - ticks_for_desired_T
                score_model["TCNT"] = bin(TCNT)

            else:
                TCNT = 255 - ticks_for_desired_T
                score_model["TCNT"] = bin(TCNT)

            if time_diff > 0 and time_diff < score["score"]:
                score = score_model

    else:
        biggest_resolution_timmer = {
            "max_time": 0,
        }
        for candidate_timmer in candidate_timmers:
            if candidate_timmer["max_time"] > biggest_resolution_timmer["max_time"]:
                biggest_resolution_timmer = candidate_timmer

        full_cycles = int(time / biggest_resolution_timmer["max_time"])
        remaining_time = time % biggest_resolution_timmer["max_time"]
        ticks_for_remaining_T = int(
            remaining_time / biggest_resolution_timmer["resolution"]
        )

        score_model = {
            "timmer": biggest_resolution_timmer["name"],
            "full_cycles": full_cycles,
            "ticks_for_remaining_T": ticks_for_remaining_T,
            "specs": biggest_resolution_timmer["specs"],
        }

        TCNT = 65535 - ticks_for_remaining_T
        score_model["TCNT"] = bin(TCNT)

        score = score_model

    print(score)


select_best_timmer(0.5)
